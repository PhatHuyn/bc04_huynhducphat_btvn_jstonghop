const ArrSo = [];

function NhapSoVaoMangB2(){
    console.log("Bài 2_1: Thêm số n vào mảng");

    var n = document.getElementById("sonB2").value * 1;
    document.getElementById("sonB2").value = "";

    ArrSo.push(n);
    console.log('Mảng n: ', ArrSo);

    document.getElementById("KQ_2_1").innerHTML = 
    `<h2>Mảng: ${ArrSo}</h2>`

    console.log("----------------------");
}

function InSoNguyenToMangB2(){
    console.log("Bài 2_2: Tìm và in ra có số nguyên tố trong mảng");
    var dem = "";
    for(var t = 0; t<ArrSo.length; t++){
        n = ArrSo[t];
        var kt = 0;
        if(n<2){
            kt++;
        }
        for(var i = 2; i<= Math.sqrt(n); i++){
            if(n % i == 0){
                kt++;
            }
        }
        if(kt == 0){
            console.log(n,"là số nguyên tố.");
            dem += n +"\t";
            document.getElementById("KQ_2_2").innerHTML = 
            `<h2>Các số nguyên tố trong mảng: ${dem}</h2>`
            console.log('các số nguyên tố trong mảng: ', dem);
        }else{
            console.log(n,"không là số nguyên tố.");
        }
    }
    console.log("----------------------");
}
