function InSoDaoNguoc(){
    console.log("Bài 5: Nhập số nguyên dương n và in số đảo ngược n.");
    var n = document.getElementById("sonB5").value * 1;
    // document.getElementById("sonB5").value = "";
    console.log('n: ', n);
    var d = 0;    

    if(n>0){
        do{
            d = d * 10 + (n % 10);
            n = Math.floor(n / 10);        
        }while(n != 0)
        document.getElementById("KQ_5").innerHTML = `Số đảo ngược là: ${d}`
    }else{
        document.getElementById("KQ_5").innerHTML = `Nhập n lớn hơn 0`
    }
    console.log("----------------------");
}