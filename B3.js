function TinhTongB3(){
    console.log("Bài 3: Nhập tham số n. Tính S=(2+3+4...+n)+2n");
    var n = document.getElementById("sonB3").value * 1;
    document.getElementById("sonB3").value = "";
    var sum = 0;
    if(n >= 2){
        for(var i = 2;i<=n;i++){
            sum += i;
        }
        sum += (2 * n);
        console.log(sum);
        document.getElementById("KQ_3").innerHTML = `S = ${sum}`
    }else{
        document.getElementById("KQ_3").innerHTML = `Nhập n lớn hơn hoặc bằng 2.`
        console.log("n phải lớn hơn 2");
    }
    console.log("----------------------");
}